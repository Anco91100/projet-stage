#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: GROUPE
#------------------------------------------------------------

CREATE TABLE GROUPE(
			IDGROUPE      Int  Auto_increment  NOT NULL ,
			LIBELLEGROUPE Varchar (4096) NOT NULL ,
			GROUPEPRIVE   Bool NOT NULL
,CONSTRAINT GROUPE_PK PRIMARY KEY (IDGROUPE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: THEME
#------------------------------------------------------------

CREATE TABLE THEME(
			IDTHEME      Int  Auto_increment  NOT NULL ,
			LIBELLETHEME Varchar (1024) NOT NULL ,
			IDGROUPE     Int NOT NULL
,CONSTRAINT THEME_PK PRIMARY KEY (IDTHEME)

,CONSTRAINT THEME_GROUPE_FK FOREIGN KEY (IDGROUPE) REFERENCES GROUPE(IDGROUPE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: VILLE
#------------------------------------------------------------

CREATE TABLE VILLE(
			IDVILLE  Int  Auto_increment  NOT NULL ,
			CPVILLE  Varchar (256) NOT NULL ,
			NOMVILLE Varchar (256) NOT NULL
,CONSTRAINT VILLE_PK PRIMARY KEY (IDVILLE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: QUARTIER
#------------------------------------------------------------

CREATE TABLE QUARTIER(
			IDQUARTIER  Int  Auto_increment  NOT NULL ,
			NOMQUARTIER Varchar (1024) NOT NULL ,
			IDVILLE     Int NOT NULL
,CONSTRAINT QUARTIER_PK PRIMARY KEY (IDQUARTIER)

,CONSTRAINT QUARTIER_VILLE_FK FOREIGN KEY (IDVILLE) REFERENCES VILLE(IDVILLE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: RUE
#------------------------------------------------------------

CREATE TABLE RUE(
			IDRUE      Int  Auto_increment  NOT NULL ,
			LIBELLERUE Varchar (4096) NOT NULL ,
			IDVILLE    Int NOT NULL
,CONSTRAINT RUE_PK PRIMARY KEY (IDRUE)

,CONSTRAINT RUE_VILLE_FK FOREIGN KEY (IDVILLE) REFERENCES VILLE(IDVILLE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ADRESSE
#------------------------------------------------------------

CREATE TABLE ADRESSE(
			IDADRESSE         Int  Auto_increment  NOT NULL ,
			NUMERORUEADRESSE  Varchar (16) ,
			NOMRUEADRESSE     Varchar (1024) NOT NULL ,
			VILLEADRESSE      Varchar (1024) NOT NULL ,
			CODEPOSTALADRESSE Varchar (128) NOT NULL ,
			QUARTIERADRESSE   Varchar (128)
,CONSTRAINT ADRESSE_PK PRIMARY KEY (IDADRESSE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: CITOYEN
#------------------------------------------------------------

CREATE TABLE CITOYEN(
			IDCITOYEN            Int  Auto_increment  NOT NULL ,
			NOMCITOYEN           Varchar (256) NOT NULL ,
			PRENOMCITOYEN        Varchar (256) NOT NULL ,
			TELCITOYEN           Varchar (256) ,
			NAISSANCECITOYEN     Date ,
			MOTDEPASSECITOYEN    Varchar (256) NOT NULL ,
			VALIDATIONCITOYEN    Bool NOT NULL ,
			CLEVALIDATIONCITOYEN Varchar (128) NOT NULL ,
			COURRIELCITOYEN      Varchar (512) NOT NULL ,
			IDADRESSE            Int
,CONSTRAINT CITOYEN_AK UNIQUE (COURRIELCITOYEN)
,CONSTRAINT CITOYEN_PK PRIMARY KEY (IDCITOYEN)

,CONSTRAINT CITOYEN_ADRESSE_FK FOREIGN KEY (IDADRESSE) REFERENCES ADRESSE(IDADRESSE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: PROPOSITION
#------------------------------------------------------------

CREATE TABLE PROPOSITION(
			IDPROPOSITION     Int  Auto_increment  NOT NULL ,
			TITREPROPOSITION  Varchar (1024) NOT NULL ,
			DETAILPROPOSITION Varchar (4096) ,
			DATEPROPOSITION   Date NOT NULL ,
			IDCITOYEN         Int NOT NULL ,
			IDTHEME           Int NOT NULL ,
			IDGROUPE          Int NOT NULL
,CONSTRAINT PROPOSITION_PK PRIMARY KEY (IDPROPOSITION)

,CONSTRAINT PROPOSITION_CITOYEN_FK FOREIGN KEY (IDCITOYEN) REFERENCES CITOYEN(IDCITOYEN)
,CONSTRAINT PROPOSITION_THEME0_FK FOREIGN KEY (IDTHEME) REFERENCES THEME(IDTHEME)
,CONSTRAINT PROPOSITION_GROUPE1_FK FOREIGN KEY (IDGROUPE) REFERENCES GROUPE(IDGROUPE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: CITOYEN_VOTE_PROPOSITION
#------------------------------------------------------------

CREATE TABLE CITOYEN_VOTE_PROPOSITION(
			IDPROPOSITION Int NOT NULL ,
			IDCITOYEN     Int NOT NULL ,
			VOTE          Bool
,CONSTRAINT CITOYEN_VOTE_PROPOSITION_PK PRIMARY KEY (IDPROPOSITION,IDCITOYEN)

,CONSTRAINT CITOYEN_VOTE_PROPOSITION_PROPOSITION_FK FOREIGN KEY (IDPROPOSITION) REFERENCES PROPOSITION(IDPROPOSITION)
,CONSTRAINT CITOYEN_VOTE_PROPOSITION_CITOYEN0_FK FOREIGN KEY (IDCITOYEN) REFERENCES CITOYEN(IDCITOYEN)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: CITOYEN_APPARTIENT_GROUPE
#------------------------------------------------------------

CREATE TABLE CITOYEN_APPARTIENT_GROUPE(
			IDGROUPE  Int NOT NULL ,
			IDCITOYEN Int NOT NULL
,CONSTRAINT CITOYEN_APPARTIENT_GROUPE_PK PRIMARY KEY (IDGROUPE,IDCITOYEN)

,CONSTRAINT CITOYEN_APPARTIENT_GROUPE_GROUPE_FK FOREIGN KEY (IDGROUPE) REFERENCES GROUPE(IDGROUPE)
,CONSTRAINT CITOYEN_APPARTIENT_GROUPE_CITOYEN0_FK FOREIGN KEY (IDCITOYEN) REFERENCES CITOYEN(IDCITOYEN)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: CITOYEN_ADMINISTRE_GROUPE
#------------------------------------------------------------

CREATE TABLE CITOYEN_ADMINISTRE_GROUPE(
			IDCITOYEN Int NOT NULL ,
			IDGROUPE  Int NOT NULL
,CONSTRAINT CITOYEN_ADMINISTRE_GROUPE_PK PRIMARY KEY (IDCITOYEN,IDGROUPE)

,CONSTRAINT CITOYEN_ADMINISTRE_GROUPE_CITOYEN_FK FOREIGN KEY (IDCITOYEN) REFERENCES CITOYEN(IDCITOYEN)
,CONSTRAINT CITOYEN_ADMINISTRE_GROUPE_GROUPE0_FK FOREIGN KEY (IDGROUPE) REFERENCES GROUPE(IDGROUPE)
)ENGINE=InnoDB;
