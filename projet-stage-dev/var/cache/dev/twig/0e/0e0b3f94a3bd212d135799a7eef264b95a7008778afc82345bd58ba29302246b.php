<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/inscription.html.twig */
class __TwigTemplate_0637afd9a5a3129311eab48f40ea645bf55aaf9cd2882e24e6cca8ffa8593d06 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/inscription.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/inscription.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/inscription.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tInscription
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 9, $this->source); })()), 'form_start');
        echo "
\t";
        // line 10
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 10, $this->source); })()), 'form_start');
        echo "

\t";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 12, $this->source); })()), "nomCitoyen", [], "any", false, false, false, 12), 'row');
        echo "
\t";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 13, $this->source); })()), "prenomCitoyen", [], "any", false, false, false, 13), 'row');
        echo "
\t";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 14, $this->source); })()), "courrielCitoyen", [], "any", false, false, false, 14), 'row');
        echo "
\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 15, $this->source); })()), "motDePasseCitoyen", [], "any", false, false, false, 15), 'row');
        echo "
\t";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 16, $this->source); })()), "confirm_mdp", [], "any", false, false, false, 16), 'row');
        echo "
\t";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 17, $this->source); })()), "_token", [], "any", false, false, false, 17), 'row');
        echo "

\t";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 19, $this->source); })()), "numeroAdresse", [], "any", false, false, false, 19), 'row');
        echo "
\t";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 20, $this->source); })()), "nomRueAdresse", [], "any", false, false, false, 20), 'row');
        echo "
\t";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 21, $this->source); })()), "codePostalAdresse", [], "any", false, false, false, 21), 'row');
        echo "
\t";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 22, $this->source); })()), "villeAdresse", [], "any", false, false, false, 22), 'row');
        echo "
\t";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 23, $this->source); })()), "_token", [], "any", false, false, false, 23), 'row');
        echo "

\t<button type=\"submit\" class=\"btn btn-success\">
\t\tInscription
\t</button>
\t";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formCitoyen"]) || array_key_exists("formCitoyen", $context) ? $context["formCitoyen"] : (function () { throw new RuntimeError('Variable "formCitoyen" does not exist.', 28, $this->source); })()), 'form_end');
        echo "
\t";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formAdresse"]) || array_key_exists("formAdresse", $context) ? $context["formAdresse"] : (function () { throw new RuntimeError('Variable "formAdresse" does not exist.', 29, $this->source); })()), 'form_end');
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/inscription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 29,  151 => 28,  143 => 23,  139 => 22,  135 => 21,  131 => 20,  127 => 19,  122 => 17,  118 => 16,  114 => 15,  110 => 14,  106 => 13,  102 => 12,  97 => 10,  93 => 9,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tInscription
{% endblock %}

{% block body %}

\t{{form_start(formCitoyen)}}
\t{{form_start(formAdresse)}}

\t{{form_row(formCitoyen.nomCitoyen)}}
\t{{form_row(formCitoyen.prenomCitoyen)}}
\t{{form_row(formCitoyen.courrielCitoyen)}}
\t{{form_row(formCitoyen.motDePasseCitoyen)}}
\t{{form_row(formCitoyen.confirm_mdp)}}
\t{{ form_row(formCitoyen._token) }}

\t{{form_row(formAdresse.numeroAdresse)}}
\t{{form_row(formAdresse.nomRueAdresse)}}
\t{{form_row(formAdresse.codePostalAdresse)}}
\t{{form_row(formAdresse.villeAdresse)}}
\t{{ form_row(formAdresse._token) }}

\t<button type=\"submit\" class=\"btn btn-success\">
\t\tInscription
\t</button>
\t{{form_end(formCitoyen)}}
\t{{form_end(formAdresse)}}

{% endblock %}
", "user/inscription.html.twig", "C:\\wamp64\\www\\projet-stage\\templates\\user\\inscription.html.twig");
    }
}
