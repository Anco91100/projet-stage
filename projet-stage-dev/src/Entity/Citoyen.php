<?php

namespace App\Entity;

use App\Repository\CitoyenRepository;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as ASSERT;

/**
 * @ORM\Entity(repositoryClass=CitoyenRepository::class)
 */
class Citoyen implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomCitoyen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenomCitoyen;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telCitoyen;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $naissanceCitoyen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motDePasseCitoyen;

    /**
     * @ASSERT\EqualTo(propertyPath= "motDePasseCitoyen",message = "Les mots de passe ne sont pas pareil")
     */
    public $confirm_mdp;

    /**
     * @ORM\ManyToOne(targetEntity=Adresse::class, inversedBy="citoyens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $courrielCitoyen;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCitoyen(): ?string
    {
        return $this->nomCitoyen;
    }

    public function setNomCitoyen(string $nomCitoyen): self
    {
        $this->nomCitoyen = $nomCitoyen;

        return $this;
    }

    public function getPrenomCitoyen(): ?string
    {
        return $this->prenomCitoyen;
    }

    public function getUsername(): ?string
    {
        return $this->prenomCitoyen;
    }

    public function setPrenomCitoyen(string $prenomCitoyen): self
    {
        $this->prenomCitoyen = $prenomCitoyen;

        return $this;
    }

    public function getTelCitoyen(): ?string
    {
        return $this->telCitoyen;
    }

    public function setTelCitoyen(?string $telCitoyen): self
    {
        $this->telCitoyen = $telCitoyen;

        return $this;
    }

    public function getNaissanceCitoyen(): ?\DateTimeInterface
    {
        return $this->naissanceCitoyen;
    }

    public function setNaissanceCitoyen(?\DateTimeInterface $naissanceCitoyen): self
    {
        $this->naissanceCitoyen = $naissanceCitoyen;

        return $this;
    }

    public function getMotDePasseCitoyen(): ?string
    {
        return $this->motDePasseCitoyen;
    }

    public function getPassword(): ?string
    {
        return $this->motDePasseCitoyen;
    }

    public function setMotDePasseCitoyen(string $motDePasseCitoyen): self
    {
        $this->motDePasseCitoyen = $motDePasseCitoyen;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCourrielCitoyen(): ?string
    {
        return $this->courrielCitoyen;
    }

    public function setCourrielCitoyen(string $courrielCitoyen): self
    {
        $this->courrielCitoyen = $courrielCitoyen;

        return $this;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getSalt()
    {
        // leaving blank - I don't need/have a password!
    }
    public function eraseCredentials()
    {
        // leaving blank - I don't need/have a password!
    }

    public function setRoles(array $roles)
    {
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
        foreach ($roles as $role) {
            if (substr($role, 0, 5) !== 'ROLE_') {
                throw new InvalidArgumentException("Chaque rôle doit commencer par 'ROLE_'");
            }
        }
        $this->roles = $roles;
        return $this;
    }
}
