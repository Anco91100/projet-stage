<?php

namespace App\Entity;

use App\Repository\AdresseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseRepository::class)
 */
class Adresse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $numeroAdresse;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $nomRueAdresse;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $villeAdresse;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $codePostalAdresse;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $quartierAdresse;

    /**
     * @ORM\OneToMany(targetEntity=Citoyen::class, mappedBy="adresse")
     */
    private $citoyens;

    public function __construct()
    {
        $this->citoyens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroAdresse(): ?string
    {
        return $this->numeroAdresse;
    }

    public function setNumeroAdresse(?string $numeroAdresse): self
    {
        $this->numeroAdresse = $numeroAdresse;

        return $this;
    }

    public function getNomRueAdresse(): ?string
    {
        return $this->nomRueAdresse;
    }

    public function setNomRueAdresse(string $nomRueAdresse): self
    {
        $this->nomRueAdresse = $nomRueAdresse;

        return $this;
    }

    public function getVilleAdresse(): ?string
    {
        return $this->villeAdresse;
    }

    public function setVilleAdresse(string $villeAdresse): self
    {
        $this->villeAdresse = $villeAdresse;

        return $this;
    }

    public function getCodePostalAdresse(): ?string
    {
        return $this->codePostalAdresse;
    }

    public function setCodePostalAdresse(string $codePostalAdresse): self
    {
        $this->codePostalAdresse = $codePostalAdresse;

        return $this;
    }

    public function getQuartierAdresse(): ?string
    {
        return $this->quartierAdresse;
    }

    public function setQuartierAdresse(?string $quartierAdresse): self
    {
        $this->quartierAdresse = $quartierAdresse;

        return $this;
    }

    /**
     * @return Collection|Citoyen[]
     */
    public function getCitoyens(): Collection
    {
        return $this->citoyens;
    }

    public function addCitoyen(Citoyen $citoyen): self
    {
        if (!$this->citoyens->contains($citoyen)) {
            $this->citoyens[] = $citoyen;
            $citoyen->setAdresse($this);
        }

        return $this;
    }

    public function removeCitoyen(Citoyen $citoyen): self
    {
        if ($this->citoyens->contains($citoyen)) {
            $this->citoyens->removeElement($citoyen);
            // set the owning side to null (unless already changed)
            if ($citoyen->getAdresse() === $this) {
                $citoyen->setAdresse(null);
            }
        }

        return $this;
    }
}
