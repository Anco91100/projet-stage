<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200605003647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adresse CHANGE numero_adresse numero_adresse VARCHAR(16) DEFAULT NULL, CHANGE quartier_adresse quartier_adresse VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE citoyen ADD courriel_citoyen VARCHAR(512) NOT NULL, CHANGE tel_citoyen tel_citoyen VARCHAR(255) DEFAULT NULL, CHANGE naissance_citoyen naissance_citoyen DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adresse CHANGE numero_adresse numero_adresse VARCHAR(16) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE quartier_adresse quartier_adresse VARCHAR(128) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE citoyen DROP courriel_citoyen, CHANGE tel_citoyen tel_citoyen VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE naissance_citoyen naissance_citoyen DATETIME DEFAULT \'NULL\'');
    }
}
