<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200605002038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, numero_adresse VARCHAR(16) DEFAULT NULL, nom_rue_adresse VARCHAR(1024) NOT NULL, ville_adresse VARCHAR(1024) NOT NULL, code_postal_adresse VARCHAR(128) NOT NULL, quartier_adresse VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE citoyen ADD adresse_id INT NOT NULL, CHANGE tel_citoyen tel_citoyen VARCHAR(255) DEFAULT NULL, CHANGE naissance_citoyen naissance_citoyen DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE citoyen ADD CONSTRAINT FK_8E7EF6AC4DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('CREATE INDEX IDX_8E7EF6AC4DE7DC5C ON citoyen (adresse_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE citoyen DROP FOREIGN KEY FK_8E7EF6AC4DE7DC5C');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP INDEX IDX_8E7EF6AC4DE7DC5C ON citoyen');
        $this->addSql('ALTER TABLE citoyen DROP adresse_id, CHANGE tel_citoyen tel_citoyen VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE naissance_citoyen naissance_citoyen DATETIME DEFAULT \'NULL\'');
    }
}
