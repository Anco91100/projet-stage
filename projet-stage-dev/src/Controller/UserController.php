<?php

namespace App\Controller;

use App\Entity\Adresse;
use App\Entity\Citoyen;
use App\Form\AdresseType;
use App\Form\CitoyenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription_user")
     */
    public function inscription(Request $request, UserPasswordEncoderInterface $encoder) // Retourne la page d'inscription avec les formulaires ou redirige vers la page de connexion
    {
        $citoyen = new Citoyen();
        $adresse = new Adresse();
        $formCitoyen = $this->createForm(CitoyenType::class, $citoyen);
        $formAdresse = $this->createForm(AdresseType::class, $adresse);
        $manager = $this->getDoctrine()->getManager();
        $formAdresse->handleRequest($request);
        $formCitoyen->handleRequest($request);
        if ($formCitoyen->isSubmitted() && $formCitoyen->isValid() && $formAdresse->isSubmitted() && $formAdresse->isValid()) {
            $hash =  $encoder->encodePassword($citoyen, $citoyen->getMotDePasseCitoyen());
            $citoyen->setMotDePasseCitoyen($hash);
            $citoyen->setAdresse($adresse);
            $manager->persist($citoyen);
            $manager->persist($adresse);
            $manager->flush();

            return $this->redirectToRoute('connexion_profil');
        }
        return $this->render('user/inscription.html.twig', ['formCitoyen' => $formCitoyen->createView(), 'formAdresse' => $formAdresse->createView()]);
    }


    /**
     * @Route("/connexion",name="connexion_profil")
     */
    public function connexion() // Retourne la page de connexion
    {
        return $this->render('user/connexion.html.twig');
    }

    /**
     * @Route("/deconnexion",name="deconnexion_profil")
     */
    public function deconnexion()
    {
    }
}
