# Projet-stage
 ## **Mise en place d'une plateforme web de démocratie citoyenne en Symfony .**

  Un grand mouvement de société s’organise autour du référendum d’initiative citoyenne, mais
il y a peu d’outils facilement utilisables et surtout open source qui existent pour organiser ce
mode de démocratie citoyenne. Nous rappelons que le référendum d’initiative citoyenne
consiste à pouvoir proposer des idées, à les discuter, à pouvoir s’exprimer sur celle-ci, et enfin
de s’exprimer par un vote formel et anonyme qui conduira à l’application de cette proposition
si elle est adoptée. Ce mode de démocratie est valable également pour toutes les échelles de
décision, de la petite association, jusqu’à l’échelle d’un pays.


  Nous vous proposons de mettre en place cette plateforme permettant à n’importe qui
d’installer les outils de démocratie citoyenne pour son propre groupe, que ce soit à l’échelle
d’un club de sport par exemple, comme à l’échelle d’une commune.

  ### Prérequis : HTML,CSS,PHP,Symfony,MVC
  
  
  
  
  
  - Elongo Pamba Grady
  - Ouraghene Chahid
  - Ingauta Ryan
